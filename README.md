# settings

https://bitbucket.org/dashboard/overview
// in den Ordner Navigieren, wo die Settings von iTerm, Tmux, Vim, VSCode gespeichert werden sollen
git clone git@bitbucket.org:b4umchen/settings.git
ln -s /Users/b4umchen/dev/settings/vscode\ Settings/settings.json /Users/b4umchen/Library/Application\ Support/Code/User
ln -s /Users/b4umchen/dev/settings/zsh\ Settings/.zshrc /Users/b4umchen
ln -s /Users/b4umchen/dev/settings/tmux\ Settings/.tmux.conf /Users/b4umchen
ln -s /Users/b4umchen/dev/settings/vim\ Settings/.vimrc /Users/b4umchen