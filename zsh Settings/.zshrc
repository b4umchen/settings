# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH
export PATH=$HOME/bin:/usr/local/bin:/sbin:/usr/sbin:$PATH

# Path to your oh-my-zsh installation.
export ZSH="/Users/b4umchen/.oh-my-zsh"

# Tmux
#ZSH_TMUX_AUTOSTART='true'
#ZSH_TMUX_FIXTERM='$TERM'
#ZSH_TMUX_FIXTERM_WITH_256COLOR='screen-256color'
#ZSH_TMUX_AUTOQUIT='true'

# Virtualenvwrapper things
# export WORKON_HOME=$HOME/.virtualenvs
# export PROJECT_HOME=$HOME/Devel

# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes

plugins=(
  git
  zsh-syntax-highlighting
  zsh-autosuggestions
  zsh-completions
  elixir
)
# Completions 
autoload -U compinit && compinit

source $ZSH/oh-my-zsh.sh

# User configuration

export MANPATH="/usr/local/man:$MANPATH"

# Enable the addition of zsh hook functions
autoload -U add-zsh-hook

# ssh
export SSH_KEY_PATH="~/.ssh/rsa_id"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# Set the tab title to the current working directory before each prompt
function tabTitle () {
  window_title="\033]0;${PWD##*/}\007"
echo -ne "$window_title"
}

# Theme


# Rest

# source /usr/local/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
# source /usr/local/share/zsh-autosuggestions/zsh-autosuggestions.zsh

# Git Aliases
alias gi="git init"
alias ga="git add ."
alias gcm="git commit -m "
alias gp="git push"

# Shortcuts Aliases
#alias start="source ~/dev/shakebox/venv/bin/activate"
#alias stop="deactivate"
alias fullstack="cd ~/dev/fullstack/"
alias settings="cd ~/dev/settings/"
alias shakebox="cd ~/dev/shakebox/"
alias frontend="cd ~/dev/shakebox/apps/sb_web/assets/"
alias mixd="cd ~/dev/shakebox/ && mix do deps.get, deps.compile, compile"
alias smixd="cd ~/dev/shakebox/ && sudo mix do deps.get, deps.compile, compile"
alias mixe="cd ~/dev/shakebox/ && mix do ecto.create, ecto.migrate"
alias smixe="cd ~/dev/shakebox/ && sudo mix do ecto.create, ecto.migrate"
alias npmi="cd ~/dev/shakebox/apps/sb_web/assets/ && npm install && cd ~/dev/shakebox/"
alias snpmi="cd ~/dev/shakebox/apps/sb_web/assets/ && sudo npm install && cd ~/dev/shakebox/"
alias runs="cd ~/dev/shakebox/ && iex -S mix"
alias sruns="cd ~/dev/shakebox/ && mix do deps.get, deps.compile, compile, ecto.create, ecto.migrate && cd ~/dev/shakebox/apps/sb_web/assets/ && npm install && cd ~/dev/shakebox/ && iex -S mix"

# zsh
alias zsh="open ~/.zshrc"

# Nvim
alias v="nvim"
alias vi="nvim"
alias init="nvim ~/.vimrc"


# weitere Aliases
alias python=/usr/local/bin/python3.7
alias pip=/usr/local/bin/pip3

# open sublime text
function code {
    if [[ $# = 0 ]]
    then
        open -a "Visual Studio Code"
    else
        local argPath="$1"
        [[ $1 = /* ]] && argPath="$1" || argPath="$PWD/${1#./}"
        open -a "Visual Studio Code" "$argPath"
    fi
}

export PATH="/usr/local/opt/libxslt/bin:$PATH"

#. $HOME/.asdf/asdf.sh

#. $HOME/.asdf/completions/asdf.bash

source "/Users/b4umchen/.oh-my-zsh/custom/themes/spaceship.zsh-theme"


. /usr/local/opt/asdf/asdf.sh

. /usr/local/opt/asdf/etc/bash_completion.d/asdf.bash


source "/Users/b4umchen/.oh-my-zsh/custom/themes/spaceship.zsh-theme"

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
source ~/powerlevel10k/powerlevel10k.zsh-theme
